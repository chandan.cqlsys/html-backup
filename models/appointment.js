/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('appointment', {
		id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'id'
		},
		email: {
			type: DataTypes.STRING(20),
			allowNull: true,
			field: 'email'
		},
		phone: {
			type: DataTypes.STRING(20),
			allowNull: true,
			field: 'phone'
		},
		startDate: {
			type: DataTypes.STRING(20),
			allowNull: true,
			field: 'start_date'
		},
		startTime: {
			type: DataTypes.STRING(20),
			allowNull: true,
			field: 'start_time'
		},
		endDate: {
			type: DataTypes.STRING(20),
			allowNull: true,
			field: 'end_date'
		},
		endTime: {
			type: DataTypes.STRING(20),
			allowNull: true,
			field: 'end_time'
		},
		location: {
			type: DataTypes.STRING(20),
			allowNull: true,
			field: 'location'
		},
		note: {
			type: DataTypes.TEXT,
			allowNull: true,
			field: 'note'
		},
		status: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'status'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'created_at'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'updated_at'
		}
	}, {
		tableName: 'appointment'
	});
};
