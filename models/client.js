/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('client', {
		id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'id'
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'name'
		},
		email: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'email'
		},
		billingAddress: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'billing_address'
		},
		contactName: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'contact_name'
		},
		phone: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'phone'
		},
		taxNumber: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'tax_number'
		},
		customPaymentId: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'custom_payment_id'
		},
		userId: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'user_id'
		},
		notes: {
			type: DataTypes.TEXT,
			allowNull: true,
			field: 'notes'
		},
		deviceToken: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'device_token'
		},
		deviceType: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'device_type'
		},
		authKey: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'auth_key'
		},
		status: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'status'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'created_at'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'updated_at'
		}
	}, {
		tableName: 'client'
	});
};
