/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('userexpense', {
		id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'id'
		},
		userId: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			field: 'user_id'
		},
		merchant: {
			type: DataTypes.STRING(255),
			allowNull: false,
			field: 'merchant'
		},
		category: {
			type: DataTypes.STRING(255),
			allowNull: false,
			field: 'category'
		},
		description: {
			type: DataTypes.TEXT,
			allowNull: false,
			field: 'description'
		},
		date: {
			type: DataTypes.STRING(255),
			allowNull: false,
			field: 'date'
		},
		photo: {
			type: DataTypes.STRING(255),
			allowNull: false,
			field: 'photo'
		},
		total: {
			type: DataTypes.STRING(255),
			allowNull: false,
			field: 'total'
		},
		tax: {
			type: DataTypes.STRING(255),
			allowNull: false,
			field: 'tax'
		},
		tip: {
			type: DataTypes.STRING(255),
			allowNull: false,
			field: 'tip'
		},
		status: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			field: 'status'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'created_at'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'updated_at'
		}
	}, {
		tableName: 'userexpense'
	});
};
