/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('users', {
		id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'id'
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'name'
		},
		email: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'email'
		},
		password: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'password'
		},
		phone: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'phone'
		},
		image: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'image'
		},
		deviceType: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'device_type'
		},
		deviceToken: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'device_token'
		},
		authKey: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'auth_key'
		},
		socialId: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'social_id'
		},
		socialType: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'social_type'
		},
		countryId: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'country_id'
		},
		accountType: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'account_type'
		},
		parentId: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'parent_id'
		},
		gender: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'gender'
		},
		status: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'status'
		},
		planId: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'plan_id'
		},
		notification: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'notification'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'created_at'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'updated_at'
		}
	}, {
		tableName: 'users'
	});
};
