/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('userBank', {
		id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'id'
		},
		userId: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'user_id'
		},
		accountNo: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'account_no'
		},
		ifscCode: {
			type: DataTypes.STRING(20),
			allowNull: true,
			field: 'ifsc_code'
		},
		holderName: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'holder_name'
		},
		branchCode: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'branch_code'
		},
		bankAddress: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'bank_address'
		},
		status: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'status'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'created_at'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'updated_at'
		}
	}, {
		tableName: 'user_bank'
	});
};
