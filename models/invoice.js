/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('invoice', {
		id: {
			type: DataTypes.INTEGER(10),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'id'
		},
		invoiceNumber: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'invoice_number'
		},
		userInvoiceNo: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'user_invoice_no'
		},
		itemExpenses: {
			type: "SET('I','E','B')",
			allowNull: false,
			defaultValue: 'I',
			field: 'item_expenses'
		},
		clientId: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'client_id'
		},
		bankId: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			field: 'bank_id'
		},
		userId: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'user_id'
		},
		itemId: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'item_id'
		},
		comment: {
			type: DataTypes.TEXT,
			allowNull: true,
			field: 'comment'
		},
		paymentDetails: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'payment_details'
		},
		terms: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'terms'
		},
		dueDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			field: 'due_date'
		},
		invoiceDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			field: 'invoice_date'
		},
		subtotal: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			field: 'subtotal'
		},
		commision: {
			type: DataTypes.STRING(50),
			allowNull: true,
			field: 'commision'
		},
		discount: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			field: 'discount'
		},
		discountAmount: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			defaultValue: '0.00',
			field: 'discount_amount'
		},
		taxAdditional: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'tax_additional'
		},
		taxAdditionalAmount: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'tax_additional_amount'
		},
		rbStatusCommission: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'rb_status_commission'
		},
		rbStatusDiscount: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'rb_status_discount'
		},
		rbStatusTax: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'rb_status_tax'
		},
		vatAmount: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			field: 'vat_amount'
		},
		vat: {
			type: DataTypes.STRING(50),
			allowNull: true,
			field: 'vat'
		},
		withholdingTax: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			field: 'withholding_tax'
		},
		commissionPercent: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			field: 'commission_percent'
		},
		taxOnFull: {
			type: DataTypes.STRING(100),
			allowNull: true,
			field: 'tax_on_full'
		},
		total: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			field: 'total'
		},
		paid: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			field: 'paid'
		},
		balance: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			field: 'balance'
		},
		createdBy: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'created_by'
		},
		status: {
			type: DataTypes.ENUM('0','1'),
			allowNull: true,
			field: 'status'
		},
		sendStatus: {
			type: DataTypes.ENUM('0','1'),
			allowNull: false,
			defaultValue: '0',
			field: 'send_status'
		},
		pdf: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'pdf'
		},
		paidStatus: {
			type: DataTypes.ENUM('0','1'),
			allowNull: false,
			defaultValue: '0',
			field: 'paid_status'
		},
		shipId: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'ship_id'
		},
		currency: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'currency'
		},
		useBillingAddress: {
			type: DataTypes.STRING(10),
			allowNull: true,
			field: 'use_billing_address'
		},
		shippingAddress: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'shipping_address'
		},
		commissionPerItemList: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'commission_per_item_list'
		},
		compoundTaxLabel: {
			type: DataTypes.STRING(255),
			allowNull: true,
			field: 'compound_tax_label'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'created_at'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'updated_at'
		}
	}, {
		tableName: 'invoice'
	});
};
