var Home = require('../controller/homeControl.js');
var User = require('../controller/UserControl.js');
var Client = require('../controller/ClientControl.js');
var Item = require('../controller/ItemControl.js');
var Expense = require('../controller/expenseControl.js');
var Bank = require('../controller/user_bankControl.js');
var Appointment = require('../controller/AppointmentControl.js');
var Invoice = require('../controller/InvoiceControl.js');
module.exports =function(app){
    // ================    login ===================
    app.route('/home').get(Home.index);
    app.route('/').get(Home.index);
    app.route('/features').get(Home.feature);
    app.route('/plans').get(Home.plans);
    app.route('/about').get(Home.about);
    app.route('/support').get(Home.support);
    app.route('/get-country').get(User.GetCountry);
    app.route('/login').get(User.Login);
    app.route('/user_login').post(User.UserLogin);
    app.route('/signup').post(User.RegisterUser);
    app.route('/dashboard/login').get(User.Login);

//  login 
    app.route('/dashboard').get(User.Dashboard);
    app.route('/logout').get(User.Logout);
    app.route('/profile').get(User.profile);
    app.route('/admin-edit').get(User.edit);
    app.route('/admin_edit').post(User.admin_edit);

// users   
    app.route('/users').get(Client.index);
    app.route('/user-add').get(Client.add);
    app.route('/user_add').post(Client.user_add);
    app.route('/delete_user').delete(Client.delete_user);
    app.route('/user_update_status').post(Client.update_status);
    app.route('/user-edit').get(Client.edit);
    app.route('/user_edit').post(Client.user_edit);

 // item   
    app.route('/items').get(Item.index);
    app.route('/item-add').get(Item.add);
    app.route('/item_add').post(Item.item_add);
    app.route('/delete_item').delete(Item.delete_item);
    app.route('/item_update_status').post(Item.update_status);
    app.route('/item-edit').get(Item.edit);
    app.route('/item_edit').post(Item.item_edit);


 // expense   
    app.route('/expenses').get(Expense.index);
    app.route('/expense-add').get(Expense.add);
    app.route('/expense_add').post(Expense.expense_add);
    app.route('/delete_expense').delete(Expense.delete_expense);
    app.route('/expense_update_status').post(Expense.update_status);
    app.route('/expense-edit').get(Expense.edit);
    app.route('/expense_edit').post(Expense.expense_edit);

// expense   
    app.route('/banks').get(Bank.index);
    app.route('/bank-add').get(Bank.add);
    app.route('/bank_add').post(Bank.bank_add);
    app.route('/delete_bank').delete(Bank.delete_bank);
    app.route('/bank_update_status').post(Bank.update_status);
    app.route('/bank-edit').get(Bank.edit);
    app.route('/bank_edit').post(Bank.bank_edit);

// appointment   
    app.route('/appointment').get(Appointment.index);
    app.route('/appointment-add').get(Appointment.add);
    app.route('/appointment_add').post(Appointment.appointment_add);
    app.route('/delete_appointment').delete(Appointment.delete_appointment);
    app.route('/appointment_update_status').post(Appointment.update_status);
    app.route('/appointment-edit').get(Appointment.edit);
    app.route('/appointment_edit').post(Appointment.appointment_edit);

// invoice   
    app.route('/invoice').get(Invoice.index);
    app.route('/invoice-add').get(Invoice.add);
    app.route('/invoice_add').post(Invoice.invoice_add);
    app.route('/delete_invoice').delete(Invoice.delete_invoice);
    app.route('/invoice_update_status').post(Invoice.update_status);
    app.route('/invoice-edit').get(Invoice.edit);
    app.route('/invoice_edit').post(Invoice.invoice_edit);


	
}
