-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 24, 2020 at 06:20 PM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.25-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inv`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int(11) NOT NULL,
  `email` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `start_date` varchar(20) NOT NULL,
  `start_time` varchar(20) NOT NULL,
  `end_date` varchar(20) NOT NULL,
  `end_time` varchar(20) NOT NULL,
  `note` text NOT NULL,
  `location` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `email`, `phone`, `start_date`, `start_time`, `end_date`, `end_time`, `note`, `location`, `status`, `created_at`, `updated_at`) VALUES
(2, 'superadmin@gmail.com', '582582582582', '2020-01-17', '05:01', '2020-01-18', '02:01', 'asdadad', 'mohali', 1, '2020-01-16 13:25:24', '2020-01-16 13:27:44');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `billing_address` varchar(255) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `tax_number` varchar(255) NOT NULL,
  `custom_payment_id` int(11) NOT NULL,
  `notes` text NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_type` int(11) NOT NULL COMMENT '0=android,1=ios',
  `auth_key` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `user_id`, `name`, `email`, `billing_address`, `contact_name`, `phone`, `tax_number`, `custom_payment_id`, `notes`, `device_token`, `device_type`, `auth_key`, `status`, `created_at`, `updated_at`) VALUES
(3, 1, 'qwdw', 'admin@code9help.com', 'qwdsqw', 'dwed', '582582582582', '325468', 63, 'drgs', '', 0, '', 0, '2019-12-27 13:34:21', '2019-12-27 13:38:47');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `status` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `status`, `photo`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 1, 'http://202.164.42.226/staging/express/admin/upload/afgani.png', 93),
(2, 'AL', 'Albania', 1, 'http://202.164.42.226/staging/express/admin/upload/albaina.png', 355),
(3, 'DZ', 'Algeria', 1, 'http://202.164.42.226/staging/express/admin/upload/algeria.png', 213),
(4, 'AS', 'American Samoa', 0, '', 1684),
(5, 'AD', 'Andorra', 0, '', 376),
(6, 'AO', 'Angola', 0, '', 244),
(7, 'AI', 'Anguilla', 0, '', 1264),
(8, 'AQ', 'Antarctica', 0, '', 0),
(9, 'AG', 'Antigua And Barbuda', 0, '', 1268),
(10, 'AR', 'Argentina', 0, '', 54),
(11, 'AM', 'Armenia', 0, '', 374),
(12, 'AW', 'Aruba', 0, '', 297),
(13, 'AU', 'Australia', 0, '', 61),
(14, 'AT', 'Austria', 0, '', 43),
(15, 'AZ', 'Azerbaijan', 0, '', 994),
(16, 'BS', 'Bahamas The', 0, '', 1242),
(17, 'BH', 'Bahrain', 1, 'http://202.164.42.226/staging/express/admin/upload/barain.png', 973),
(18, 'BD', 'Bangladesh', 0, '', 880),
(19, 'BB', 'Barbados', 0, '', 1246),
(20, 'BY', 'Belarus', 0, '', 375),
(21, 'BE', 'Belgium', 0, '', 32),
(22, 'BZ', 'Belize', 0, '', 501),
(23, 'BJ', 'Benin', 0, '', 229),
(24, 'BM', 'Bermuda', 0, '', 1441),
(25, 'BT', 'Bhutan', 0, '', 975),
(26, 'BO', 'Bolivia', 0, '', 591),
(27, 'BA', 'Bosnia and Herzegovina', 0, '', 387),
(28, 'BW', 'Botswana', 0, '', 267),
(29, 'BV', 'Bouvet Island', 0, '', 0),
(30, 'BR', 'Brazil', 0, '', 55),
(31, 'IO', 'British Indian Ocean Territory', 0, '', 246),
(32, 'BN', 'Brunei', 0, '', 673),
(33, 'BG', 'Bulgaria', 0, '', 359),
(34, 'BF', 'Burkina Faso', 0, '', 226),
(35, 'BI', 'Burundi', 0, '', 257),
(36, 'KH', 'Cambodia', 0, '', 855),
(37, 'CM', 'Cameroon', 0, '', 237),
(38, 'CA', 'Canada', 1, 'http://202.164.42.226/staging/express/admin/upload/caneda.png', 1),
(39, 'CV', 'Cape Verde', 0, '', 238),
(40, 'KY', 'Cayman Islands', 0, '', 1345),
(41, 'CF', 'Central African Republic', 0, '', 236),
(42, 'TD', 'Chad', 0, '', 235),
(43, 'CL', 'Chile', 0, '', 56),
(44, 'CN', 'China', 1, 'http://202.164.42.226/staging/express/admin/upload/chaina.png', 86),
(45, 'CX', 'Christmas Island', 0, '', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 0, '', 672),
(47, 'CO', 'Colombia', 0, '', 57),
(48, 'KM', 'Comoros', 0, '', 269),
(49, 'CG', 'Republic Of The Congo', 0, '', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 0, '', 242),
(51, 'CK', 'Cook Islands', 0, '', 682),
(52, 'CR', 'Costa Rica', 0, '', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 0, '', 225),
(54, 'HR', 'Croatia (Hrvatska)', 0, '', 385),
(55, 'CU', 'Cuba', 0, '', 53),
(56, 'CY', 'Cyprus', 0, '', 357),
(57, 'CZ', 'Czech Republic', 0, '', 420),
(58, 'DK', 'Denmark', 0, '', 45),
(59, 'DJ', 'Djibouti', 0, '', 253),
(60, 'DM', 'Dominica', 0, '', 1767),
(61, 'DO', 'Dominican Republic', 0, '', 1809),
(62, 'TP', 'East Timor', 0, '', 670),
(63, 'EC', 'Ecuador', 0, '', 593),
(64, 'EG', 'Egypt', 1, 'http://202.164.42.226/staging/express/admin/upload/egypt.png', 20),
(65, 'SV', 'El Salvador', 0, '', 503),
(66, 'GQ', 'Equatorial Guinea', 0, '', 240),
(67, 'ER', 'Eritrea', 0, '', 291),
(68, 'EE', 'Estonia', 0, '', 372),
(69, 'ET', 'Ethiopia', 0, '', 251),
(70, 'XA', 'External Territories of Australia', 0, '', 61),
(71, 'FK', 'Falkland Islands', 0, '', 500),
(72, 'FO', 'Faroe Islands', 0, '', 298),
(73, 'FJ', 'Fiji Islands', 0, '', 679),
(74, 'FI', 'Finland', 0, '', 358),
(75, 'FR', 'France', 1, 'http://202.164.42.226/staging/express/admin/upload/france.png', 33),
(76, 'GF', 'French Guiana', 0, '', 594),
(77, 'PF', 'French Polynesia', 0, '', 689),
(78, 'TF', 'French Southern Territories', 0, '', 0),
(79, 'GA', 'Gabon', 0, '', 241),
(80, 'GM', 'Gambia The', 0, '', 220),
(81, 'GE', 'Georgia', 0, '', 995),
(82, 'DE', 'Germany', 0, '', 49),
(83, 'GH', 'Ghana', 0, '', 233),
(84, 'GI', 'Gibraltar', 0, '', 350),
(85, 'GR', 'Greece', 0, '', 30),
(86, 'GL', 'Greenland', 0, '', 299),
(87, 'GD', 'Grenada', 0, '', 1473),
(88, 'GP', 'Guadeloupe', 0, '', 590),
(89, 'GU', 'Guam', 0, '', 1671),
(90, 'GT', 'Guatemala', 0, '', 502),
(91, 'XU', 'Guernsey and Alderney', 0, '', 44),
(92, 'GN', 'Guinea', 0, '', 224),
(93, 'GW', 'Guinea-Bissau', 0, '', 245),
(94, 'GY', 'Guyana', 0, '', 592),
(95, 'HT', 'Haiti', 0, '', 509),
(96, 'HM', 'Heard and McDonald Islands', 0, '', 0),
(97, 'HN', 'Honduras', 0, '', 504),
(98, 'HK', 'Hong Kong S.A.R.', 1, 'http://202.164.42.226/staging/express/admin/upload/hong-cong.png', 852),
(99, 'HU', 'Hungary', 0, '', 36),
(100, 'IS', 'Iceland', 0, '', 354),
(101, 'IN', 'India', 1, 'http://202.164.42.226/staging/express/admin/upload/in.svg', 91),
(102, 'ID', 'Indonesia', 0, '', 62),
(103, 'IR', 'Iran', 1, 'http://202.164.42.226/staging/express/admin/upload/iran.png', 98),
(104, 'IQ', 'Iraq', 1, 'http://202.164.42.226/staging/express/admin/upload/iraq.png', 964),
(105, 'IE', 'Ireland', 0, '', 353),
(106, 'IL', 'Israel', 0, '', 972),
(107, 'IT', 'Italy', 0, '', 39),
(108, 'JM', 'Jamaica', 0, '', 1876),
(109, 'JP', 'Japan', 0, '', 81),
(110, 'XJ', 'Jersey', 0, '', 44),
(111, 'JO', 'Jordan', 1, 'http://202.164.42.226/staging/express/admin/upload/jordan.png', 962),
(112, 'KZ', 'Kazakhstan', 0, '', 7),
(113, 'KE', 'Kenya', 0, '', 254),
(114, 'KI', 'Kiribati', 0, '', 686),
(115, 'KP', 'Korea North', 0, '', 850),
(116, 'KR', 'Korea South', 0, '', 82),
(117, 'KW', 'Kuwait', 1, 'http://202.164.42.226/staging/express/admin/upload/kuvet.png', 965),
(118, 'KG', 'Kyrgyzstan', 0, '', 996),
(119, 'LA', 'Laos', 0, '', 856),
(120, 'LV', 'Latvia', 0, '', 371),
(121, 'LB', 'Lebanon', 1, 'http://202.164.42.226/staging/express/admin/upload/lebanan.png', 961),
(122, 'LS', 'Lesotho', 0, '', 266),
(123, 'LR', 'Liberia', 0, '', 231),
(124, 'LY', 'Libya', 1, 'http://202.164.42.226/staging/express/admin/upload/libeya.png', 218),
(125, 'LI', 'Liechtenstein', 0, '', 423),
(126, 'LT', 'Lithuania', 0, '', 370),
(127, 'LU', 'Luxembourg', 0, '', 352),
(128, 'MO', 'Macau S.A.R.', 0, '', 853),
(129, 'MK', 'Macedonia', 0, '', 389),
(130, 'MG', 'Madagascar', 0, '', 261),
(131, 'MW', 'Malawi', 0, '', 265),
(132, 'MY', 'Malaysia', 0, '', 60),
(133, 'MV', 'Maldives', 0, '', 960),
(134, 'ML', 'Mali', 0, '', 223),
(135, 'MT', 'Malta', 0, '', 356),
(136, 'XM', 'Man (Isle of)', 0, '', 44),
(137, 'MH', 'Marshall Islands', 0, '', 692),
(138, 'MQ', 'Martinique', 0, '', 596),
(139, 'MR', 'Mauritania', 1, 'http://202.164.42.226/staging/express/admin/upload/muritania.png', 222),
(140, 'MU', 'Mauritius', 0, '', 230),
(141, 'YT', 'Mayotte', 0, '', 269),
(142, 'MX', 'Mexico', 1, 'http://202.164.42.226/staging/express/admin/upload/mco.png', 52),
(143, 'FM', 'Micronesia', 0, '', 691),
(144, 'MD', 'Moldova', 0, '', 373),
(145, 'MC', 'Monaco', 0, '', 377),
(146, 'MN', 'Mongolia', 0, '', 976),
(147, 'MS', 'Montserrat', 0, '', 1664),
(148, 'MA', 'Morocco', 1, 'http://202.164.42.226/staging/express/admin/upload/morocco.png', 212),
(149, 'MZ', 'Mozambique', 0, '', 258),
(150, 'MM', 'Myanmar', 0, '', 95),
(151, 'NA', 'Namibia', 0, '', 264),
(152, 'NR', 'Nauru', 0, '', 674),
(153, 'NP', 'Nepal', 0, '', 977),
(154, 'AN', 'Netherlands Antilles', 0, '', 599),
(155, 'NL', 'Netherlands The', 0, '', 31),
(156, 'NC', 'New Caledonia', 0, '', 687),
(157, 'NZ', 'New Zealand', 0, '', 64),
(158, 'NI', 'Nicaragua', 0, '', 505),
(159, 'NE', 'Niger', 0, '', 227),
(160, 'NG', 'Nigeria', 0, '', 234),
(161, 'NU', 'Niue', 0, '', 683),
(162, 'NF', 'Norfolk Island', 0, '', 672),
(163, 'MP', 'Northern Mariana Islands', 0, '', 1670),
(164, 'NO', 'Norway', 0, '', 47),
(165, 'OM', 'Oman', 1, 'http://202.164.42.226/staging/express/admin/upload/oman.png', 968),
(166, 'PK', 'Pakistan', 0, '', 92),
(167, 'PW', 'Palau', 0, '', 680),
(168, 'PS', 'Palestinian Territory Occupied', 1, 'http://202.164.42.226/staging/express/admin/upload/plastin.png', 970),
(169, 'PA', 'Panama', 0, '', 507),
(170, 'PG', 'Papua new Guinea', 0, '', 675),
(171, 'PY', 'Paraguay', 0, '', 595),
(172, 'PE', 'Peru', 0, '', 51),
(173, 'PH', 'Philippines', 0, '', 63),
(174, 'PN', 'Pitcairn Island', 0, '', 0),
(175, 'PL', 'Poland', 0, '', 48),
(176, 'PT', 'Portugal', 0, '', 351),
(177, 'PR', 'Puerto Rico', 0, '', 1787),
(178, 'QA', 'Qatar', 1, 'http://202.164.42.226/staging/express/admin/upload/qatar.png', 974),
(179, 'RE', 'Reunion', 0, '', 262),
(180, 'RO', 'Romania', 0, '', 40),
(181, 'RU', 'Russia', 0, '', 70),
(182, 'RW', 'Rwanda', 0, '', 250),
(183, 'SH', 'Saint Helena', 0, '', 290),
(184, 'KN', 'Saint Kitts And Nevis', 0, '', 1869),
(185, 'LC', 'Saint Lucia', 0, '', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 0, '', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 0, '', 1784),
(188, 'WS', 'Samoa', 0, '', 684),
(189, 'SM', 'San Marino', 0, '', 378),
(190, 'ST', 'Sao Tome and Principe', 0, '', 239),
(191, 'SA', 'Saudi Arabia', 1, 'http://202.164.42.226/staging/express/admin/upload/saudi.png', 966),
(192, 'SN', 'Senegal', 0, '', 221),
(193, 'RS', 'Serbia', 0, '', 381),
(194, 'SC', 'Seychelles', 0, '', 248),
(195, 'SL', 'Sierra Leone', 0, '', 232),
(196, 'SG', 'Singapore', 0, '', 65),
(197, 'SK', 'Slovakia', 0, '', 421),
(198, 'SI', 'Slovenia', 0, '', 386),
(199, 'XG', 'Smaller Territories of the UK', 0, '', 44),
(200, 'SB', 'Solomon Islands', 0, '', 677),
(201, 'SO', 'Somalia', 0, '', 252),
(202, 'ZA', 'South Africa', 0, '', 27),
(203, 'GS', 'South Georgia', 0, '', 0),
(204, 'SS', 'South Sudan', 0, '', 211),
(205, 'ES', 'Spain', 0, '', 34),
(206, 'LK', 'Sri Lanka', 0, '', 94),
(207, 'SD', 'Sudan', 1, 'http://202.164.42.226/staging/express/admin/upload/suda.png', 249),
(208, 'SR', 'Suriname', 0, '', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 0, '', 47),
(210, 'SZ', 'Swaziland', 0, '', 268),
(211, 'SE', 'Sweden', 0, '', 46),
(212, 'CH', 'Switzerland', 0, '', 41),
(213, 'SY', 'Syria', 1, 'http://202.164.42.226/staging/express/admin/upload/syria.png', 963),
(214, 'TW', 'Taiwan', 0, '', 886),
(215, 'TJ', 'Tajikistan', 0, '', 992),
(216, 'TZ', 'Tanzania', 0, '', 255),
(217, 'TH', 'Thailand', 0, '', 66),
(218, 'TG', 'Togo', 0, '', 228),
(219, 'TK', 'Tokelau', 0, '', 690),
(220, 'TO', 'Tonga', 0, '', 676),
(221, 'TT', 'Trinidad And Tobago', 0, '', 1868),
(222, 'TN', 'Tunisia', 1, 'http://202.164.42.226/staging/express/admin/upload/tunisa.png', 216),
(223, 'TR', 'Turkey', 0, '', 90),
(224, 'TM', 'Turkmenistan', 0, '', 7370),
(225, 'TC', 'Turks And Caicos Islands', 0, '', 1649),
(226, 'TV', 'Tuvalu', 0, '', 688),
(227, 'UG', 'Uganda', 0, '', 256),
(228, 'UA', 'Ukraine', 0, '', 380),
(229, 'AE', 'United Arab Emirates', 1, 'http://202.164.42.226/staging/express/admin/upload/use.png', 971),
(230, 'GB', 'United Kingdom', 0, '', 44),
(231, 'US', 'United States', 0, '', 1),
(232, 'UM', 'United States Minor Outlying Islands', 0, '', 1),
(233, 'UY', 'Uruguay', 0, '', 598),
(234, 'UZ', 'Uzbekistan', 0, '', 998),
(235, 'VU', 'Vanuatu', 0, '', 678),
(236, 'VA', 'Vatican City State (Holy See)', 0, '', 39),
(237, 'VE', 'Venezuela', 0, '', 58),
(238, 'VN', 'Vietnam', 0, '', 84),
(239, 'VG', 'Virgin Islands (British)', 0, '', 1284),
(240, 'VI', 'Virgin Islands (US)', 0, '', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 0, '', 681),
(242, 'EH', 'Western Sahara', 0, '', 212),
(243, 'YE', 'Yemen', 1, 'http://202.164.42.226/staging/express/admin/upload/yemen.png', 967),
(244, 'YU', 'Yugoslavia', 0, '', 38),
(245, 'ZM', 'Zambia', 0, '', 260),
(246, 'ZW', 'Zimbabwe', 0, '', 263),
(247, 'du', 'Dubai', 1, 'http://202.164.42.226/staging/express/admin/upload/dubai.png', 971);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(10) NOT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `user_invoice_no` int(11) DEFAULT NULL,
  `item_expenses` set('I','E','B') NOT NULL DEFAULT 'I',
  `client_id` int(11) DEFAULT NULL,
  `bank_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `comment` text,
  `payment_details` varchar(255) DEFAULT NULL,
  `terms` varchar(255) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `subtotal` decimal(20,2) DEFAULT NULL,
  `commision` varchar(50) DEFAULT NULL,
  `discount` decimal(20,2) DEFAULT NULL,
  `discount_amount` decimal(20,2) DEFAULT '0.00',
  `tax_additional` varchar(255) DEFAULT NULL,
  `tax_additional_amount` varchar(255) DEFAULT NULL,
  `rb_status_commission` varchar(255) DEFAULT NULL,
  `rb_status_discount` varchar(255) DEFAULT NULL,
  `rb_status_tax` varchar(255) DEFAULT NULL,
  `vat_amount` decimal(20,2) DEFAULT NULL,
  `vat` varchar(50) DEFAULT NULL,
  `withholding_tax` decimal(20,2) DEFAULT NULL,
  `commission_percent` decimal(12,0) DEFAULT NULL,
  `tax_on_full` varchar(100) DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `paid` decimal(20,2) DEFAULT NULL,
  `balance` decimal(20,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  `send_status` enum('0','1') NOT NULL DEFAULT '0',
  `pdf` varchar(255) DEFAULT NULL,
  `paid_status` enum('0','1') NOT NULL DEFAULT '0',
  `ship_id` varchar(255) DEFAULT NULL,
  `currency` int(11) DEFAULT NULL,
  `use_billing_address` varchar(10) DEFAULT NULL,
  `shipping_address` varchar(255) DEFAULT NULL,
  `commission_per_item_list` varchar(255) DEFAULT NULL,
  `compound_tax_label` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `invoice_number`, `user_id`, `user_invoice_no`, `item_expenses`, `client_id`, `bank_id`, `item_id`, `comment`, `payment_details`, `terms`, `due_date`, `invoice_date`, `subtotal`, `commision`, `discount`, `discount_amount`, `tax_additional`, `tax_additional_amount`, `rb_status_commission`, `rb_status_discount`, `rb_status_tax`, `vat_amount`, `vat`, `withholding_tax`, `commission_percent`, `tax_on_full`, `total`, `paid`, `balance`, `created_by`, `status`, `send_status`, `pdf`, `paid_status`, `ship_id`, `currency`, `use_billing_address`, `shipping_address`, `commission_per_item_list`, `compound_tax_label`, `created_at`, `updated_at`) VALUES
(32, '#-Invoices Number #2', 1, 2, 'I', 3, 0, NULL, 'ghggjg', 'hgjhgjh', '', '2019-12-17', '2019-12-17', '17.00', NULL, '0.00', '0.00', '0', NULL, '1', '1', '1', '2.90', NULL, NULL, '0', '0', '41.46', NULL, '41.46', 1, NULL, '0', NULL, '0', '1613', 6, 'Y', 'bjbb', NULL, '', '2020-01-24 10:28:33', '2020-01-24 10:29:23'),
(33, '#123-Invoices Number #2', 1, 2, 'I', 3, 0, NULL, '', '', '', '2019-12-18', '2019-12-18', '1375.00', NULL, '0.00', '0.00', '0', NULL, '1', '1', '1', '125.00', NULL, NULL, '0', '0', '1512.50', NULL, '1512.50', 1, NULL, '0', '1576625865.pdf', '0', '', 6, 'Y', '', NULL, '', '2020-01-24 10:28:33', '2020-01-24 10:29:23');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `cost` varchar(255) NOT NULL,
  `unit_type` int(11) NOT NULL COMMENT '0=hours,1=day,2=per piece',
  `tax_enable` int(11) NOT NULL COMMENT '0=no ,1=yes',
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `description`, `user_id`, `rate`, `cost`, `unit_type`, `tax_enable`, `status`, `created_at`, `updated_at`) VALUES
(2, 'demo', 'ssadsadasd', 1, '10', '15', 1, 1, 1, '2020-01-02 13:00:20', '2020-01-02 13:00:30');

-- --------------------------------------------------------

--
-- Table structure for table `userexpense`
--

CREATE TABLE `userexpense` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `merchant` varchar(255) NOT NULL,
  `category` varchar(11) NOT NULL,
  `description` text NOT NULL,
  `date` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `tax` varchar(255) NOT NULL,
  `tip` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userexpense`
--

INSERT INTO `userexpense` (`id`, `user_id`, `merchant`, `category`, `description`, `date`, `photo`, `total`, `tax`, `tip`, `status`, `created_at`, `updated_at`) VALUES
(3, 1, 'test', 'Shipping', 'ss', '2020-01-04', '84629bae-1c57-461f-a938-509e64d6089a.jpg', '12', '12', '12', 1, '2020-01-03 13:22:56', '2020-01-03 13:22:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `device_type` int(11) NOT NULL COMMENT '0=email,1=ios,2=android',
  `device_token` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `social_id` varchar(255) NOT NULL,
  `social_type` int(11) NOT NULL COMMENT '0=gmail,1=fb,2=g+	',
  `country_id` int(11) NOT NULL,
  `account_type` int(11) NOT NULL COMMENT '0=user,1=client',
  `parent_id` int(11) NOT NULL COMMENT '0=user',
  `gender` int(11) NOT NULL COMMENT 'o=male,1=female,2=other',
  `status` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `notification` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `image`, `device_type`, `device_token`, `auth_key`, `social_id`, `social_type`, `country_id`, `account_type`, `parent_id`, `gender`, `status`, `plan_id`, `notification`, `created_at`, `updated_at`) VALUES
(1, 'demo', 'admin@admin.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '582582582582', '202280d8-8125-4283-8d7c-aee0316f2885.jpg', 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, '2019-12-23 12:40:10', '2020-01-06 13:33:09'),
(3, 'admin', 'testcqlsys90@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '', 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, '2019-12-23 12:45:03', '2019-12-23 12:45:03'),
(4, 'abc', 'abc@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '', 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, '2019-12-23 13:32:09', '2019-12-23 13:32:09');

-- --------------------------------------------------------

--
-- Table structure for table `user_bank`
--

CREATE TABLE `user_bank` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_no` varchar(255) NOT NULL,
  `ifsc_code` varchar(20) NOT NULL,
  `holder_name` varchar(255) NOT NULL,
  `branch_code` varchar(255) NOT NULL,
  `bank_address` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_bank`
--

INSERT INTO `user_bank` (`id`, `user_id`, `account_no`, `ifsc_code`, `holder_name`, `branch_code`, `bank_address`, `status`, `created_at`, `updated_at`) VALUES
(4, 1, '4242424242424242', 'PNDVGE200', 'new test', 'SBI360', 'test ', 0, '2020-01-08 13:29:26', '2020-01-08 13:29:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userexpense`
--
ALTER TABLE `userexpense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_bank`
--
ALTER TABLE `user_bank`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `userexpense`
--
ALTER TABLE `userexpense`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_bank`
--
ALTER TABLE `user_bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
