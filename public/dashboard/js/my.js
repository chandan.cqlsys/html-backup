
  
function previewImage(event)
  {
    var reader = new FileReader();
    reader.onload = function()
    {
      var output = document.getElementById('image_view');
      output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
  }

  function previewImageResume(event)
  {
    var reader = new FileReader();
    reader.onload = function()
    {
      var output = document.getElementById('image_view_resume');
      output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
  }

  function previewImageInsurance(event)
  {
    var reader = new FileReader();
    reader.onload = function()
    {
      var output = document.getElementById('image_view_Insurance');
      output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
  }

   function previewImageLicence(event)
  {
    var reader = new FileReader();
    reader.onload = function()
    {
      var output = document.getElementById('image_view_Licence');
      output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
  }

   setTimeout(() => {
       $("#flash_msg_dashboard").fadeOut('slow');
       },3000);


 $(document).ready(function(){
    $.ajax({
      method:'get',
      url:'/get-country',
      success:function(data){
       //console.log(data);
       var html ='';
        html+='	<select name="Country" class="w3-select" required><option value="">Please select Country</option>';
       $.each(data,function(k,v){
        html+='<option value="'+v.id+'">'+v.name+'</option>';
       });
        html+='</select>';
        $('#country').html(html);
      }
    });

 });

 $(document).on('click','.dlt',function(){
  $(this).parents('.rem').remove();
 });
//  update status ---------------

 $(document).on('click','.update_status_user',function(){
  _this = $(this);
  var id=$(this).attr('rel');
  //alert(id);
  $.ajax({
   type:"post",
   url:"/user_update_status",
   data:{id:id},
   success:function(data){
   // console.log(data); return false;
    if (data == 1) {
            _this.removeClass('label label-danger');
            _this.addClass('label label-success');
            _this.text('Active');
                  }
          if (data == 0) {
              _this.removeClass('label label-success');
              _this.addClass('label label-danger');
              _this.text('Inactive');
          }
  
   }
  
  })
  });


//  item update status
  $(document).on('click','.update_status_item',function(){
    _this = $(this);
    var id=$(this).attr('rel');
    //alert(id);
    $.ajax({
     type:"post",
     url:"/item_update_status",
     data:{id:id},
     success:function(data){
     // console.log(data); return false;
      if (data == 1) {
              _this.removeClass('label label-danger');
              _this.addClass('label label-success');
              _this.text('Active');
                    }
            if (data == 0) {
                _this.removeClass('label label-success');
                _this.addClass('label label-danger');
                _this.text('Inactive');
            }
    
     }
    
    })
    });



    //  expense update status
  $(document).on('click','.update_status_expense',function(){
    _this = $(this);
    var id=$(this).attr('rel');
    //alert(id);
    $.ajax({
     type:"post",
     url:"/expense_update_status",
     data:{id:id},
     success:function(data){
     // console.log(data); return false;
      if (data == 1) {
              _this.removeClass('label label-danger');
              _this.addClass('label label-success');
              _this.text('Active');
                    }
            if (data == 0) {
                _this.removeClass('label label-success');
                _this.addClass('label label-danger');
                _this.text('Inactive');
            }
    
     }
    
    })
    });


    //  Bank update status
  $(document).on('click','.update_status_bank',function(){
    _this = $(this);
    var id=$(this).attr('rel');
    //alert(id);
    $.ajax({
     type:"post",
     url:"/bank_update_status",
     data:{id:id},
     success:function(data){
     // console.log(data); return false;
      if (data == 1) {
              _this.removeClass('label label-danger');
              _this.addClass('label label-success');
              _this.text('Active');
                    }
            if (data == 0) {
                _this.removeClass('label label-success');
                _this.addClass('label label-danger');
                _this.text('Inactive');
            }
    
     }
    
    })
    });

     //  Appointment update status
  $(document).on('click','.update_status_appointment',function(){
    _this = $(this);
    var id=$(this).attr('rel');
    //alert(id);
    $.ajax({
     type:"post",
     url:"/appointment_update_status",
     data:{id:id},
     success:function(data){
     // console.log(data); return false;
      if (data == 1) {
              _this.removeClass('label label-danger');
              _this.addClass('label label-success');
              _this.text('Active');
                    }
            if (data == 0) {
                _this.removeClass('label label-success');
                _this.addClass('label label-danger');
                _this.text('Inactive');
            }
    
     }
    
    })
    });

//  delete user ----------
  $(document).on('click','.user_delete',function(){
    var id=$(this).attr('rel');
    
    swal({
          title: "Are you sure?",
          text: "You want to delete this user !",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      }).then((willDelete) => {
          if(willDelete) {
      $.ajax({
         type:"delete",
            url:"/delete_user",
            data:{id:id},
           success:function(data){ 
            if(data==1){
              { 
               swal("Done!", "It was successfully deleted!", "success");
                 setTimeout(function () { location.reload(1); }, 2000);
              }
              
            }
  } 
      });
    }else
      {
          swal("Cancelled","The user is safe !","error");
      }
    
  });
  
  });


  //  delete item ----------
  $(document).on('click','.item_delete',function(){
    var id=$(this).attr('rel');
    
    swal({
          title: "Are you sure?",
          text: "You want to delete this item !",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      }).then((willDelete) => {
          if(willDelete) {
      $.ajax({
         type:"delete",
            url:"/delete_item",
            data:{id:id},
           success:function(data){ 
            if(data==1){
              { 
               swal("Done!", "It was successfully deleted!", "success");
                 setTimeout(function () { location.reload(1); }, 2000);
              }
              
            }
  } 
      });
    }else
      {
          swal("Cancelled","The item is safe !","error");
      }
    
  });
  
  });


    //  delete expense ----------
    $(document).on('click','.expense_delete',function(){
      var id=$(this).attr('rel');
      
      swal({
            title: "Are you sure?",
            text: "You want to delete this expense !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if(willDelete) {
        $.ajax({
           type:"delete",
              url:"/delete_expense",
              data:{id:id},
             success:function(data){ 
              if(data==1){
                { 
                 swal("Done!", "It was successfully deleted!", "success");
                   setTimeout(function () { location.reload(1); }, 2000);
                }
                
              }
    } 
        });
      }else
        {
            swal("Cancelled","The expense is safe !","error");
        }
      
    });
    
    });


     //  delete bank ----------
     $(document).on('click','.bank_delete',function(){
      var id=$(this).attr('rel');
      
      swal({
            title: "Are you sure?",
            text: "You want to delete this bank !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if(willDelete) {
        $.ajax({
           type:"delete",
              url:"/delete_bank",
              data:{id:id},
             success:function(data){ 
              if(data==1){
                { 
                 swal("Done!", "It was successfully deleted!", "success");
                   setTimeout(function () { location.reload(1); }, 2000);
                }
                
              }
    } 
        });
      }else
        {
            swal("Cancelled","The expense is safe !","error");
        }
      
    });
    
    });

     //  delete appointment ----------
     $(document).on('click','.appointment_delete',function(){
      var id=$(this).attr('rel');
      
      swal({
            title: "Are you sure?",
            text: "You want to delete this appointment !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
  if(willDelete) {
        $.ajax({
           type:"delete",
              url:"/delete_appointment",
              data:{id:id},
             success:function(data){ 
                  if(data==1){
                    { 
                    swal("Done!", "It was successfully deleted!", "success");
                      setTimeout(function () { location.reload(1); }, 2000);
                    }
                    
                  }
              } 
            });
      }else
        {
            swal("Cancelled","The appointment is safe !","error");
        }
      
    });
    
    });

     //  delete invoice ----------
     $(document).on('click','.invoice_delete',function(){
      var id=$(this).attr('rel');
      
      swal({
            title: "Are you sure?",
            text: "You want to delete this invoice !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
  if(willDelete) {
        $.ajax({
           type:"delete",
              url:"/delete_invoice",
              data:{id:id},
             success:function(data){ 
                  if(data==1){
                    { 
                    swal("Done!", "It was successfully deleted!", "success");
                      setTimeout(function () { location.reload(1); }, 2000);
                    }
                    
                  }
              } 
            });
      }else
        {
            swal("Cancelled","The appointment is safe !","error");
        }
      
    });
    
    });




  


 
