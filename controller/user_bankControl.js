const db= require('../models');
const sequelize=require('sequelize');
var crypto=require('crypto');
const flash = require('connect-flash');
const user_bank_bank =db.userBank;

module.exports={

index: async function(req,res){
	try{
		if(req.session && req.session.auth == true){
		var get_user_bank_bank = await user_bank_bank.findAll({
			where:{
				userId:req.session.login_id
			}
		});
		if(get_user_bank_bank){
			get_user_bank_bank =get_user_bank_bank.map(val=>{return val.toJSON()});
		}
		res.render("dashboard/user_bank/index",{title:'user_bank',msg:req.flash('msg'),data:get_user_bank_bank,session:req.session});	
	}else{
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
		}catch(error){
		throw(error);
	}
},

add: async function (req,res){
	if(req.session && req.session.auth == true){
		res.render("dashboard/user_bank/add",{title:'user_bank',msg:req.flash('msg'),session:req.session});
	}else{ 
	req.flash('msg','Please login first')
     res.redirect('/home');
	}
},

bank_add: async function (req,res){
	if(req.session && req.session.auth == true){
		 var data =req.body;
		 data.userId=req.session.login_id;
		 var insertdata = await user_bank_bank.create(data);
		 if(insertdata){
			req.flash('msg','Bank Add Successfully')
			res.redirect('/banks');
		 }
	}else{ 
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
},
delete_bank: async function (req,res){
		const dlt = await user_bank_bank.destroy({
			where: {
			  id: req.body.id
			}
		  });
     res.json(1);
},
update_status:async function(req,res){
		try{
			var id=req.body.id;
			var user_bankss = await user_bank_bank.findOne({
			where : {
			id:id
			}
			});
		var st ='';
		if(user_bankss.dataValues.status==1){
			st =0;
		}else{
			st =1;
		}
		// update query ---------------------
		var user_bank_update = await user_bank_bank.update(
			{ 
				status:st
			},
		{ 
		where: 
			{ 
			id: id 
			} 
			}
		);
		res.json(st);
		}catch(error){
			throw(error);
			}
	
		},

edit: async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var id= req.query.id;
		user_bank_data = await user_bank_bank.findOne({
			where:{
               id:id
 			}
		});
		if(user_bank_data){
			user_bank_data=user_bank_data.toJSON();
			res.render("dashboard/user_bank/edit",{msg:req.flash('msg'),response:user_bank_data,title:'user_bank',session:req.session});	
		}else{
			req.flash('msg', 'User Not Found.')
			res.redirect('/banks');
		}
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}	

	}catch(error){

		throw(error);
	}
},
bank_edit:async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var requestdata = req.body;
		var id =requestdata.id;
		var user_banks = await user_bank_bank.findOne({
			where : {
			id:requestdata.id
			}
			});
			delete requestdata.id;
		var user_bank_update = await user_bank_bank.update(
		   requestdata,
			{ 
				where: 
				{ 
					id: id 
				} 
			}
		);
		req.flash('msg', 'bank updated successfully')

		res.redirect('/banks')
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}		

	}catch(error){
		throw(error)
	}

},

}
