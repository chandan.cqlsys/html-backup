const db= require('../models');
const sequelize=require('sequelize');
var crypto=require('crypto');
const flash = require('connect-flash');
const appointment =db.appointment;
const country =db.countries;
module.exports={

index: async function(req,res){
	try{
		if(req.session && req.session.auth == true){
		var get_appointment = await appointment.findAll({
		});
		if(get_appointment){
			get_appointment =get_appointment.map(val=>{return val.toJSON()});
		}
		res.render("dashboard/appointment/index",{title:'appointment',msg:req.flash('msg'),data:get_appointment,session:req.session});	
	}else{
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
		}catch(error){
		throw(error);
	}
},

add: async function (req,res){
	if(req.session && req.session.auth == true){
		res.render("dashboard/appointment/add",{title:'appointment',msg:req.flash('msg'),session:req.session});
	}else{ 
	req.flash('msg','Please login first')
     res.redirect('/home');
	}
},

appointment_add: async function (req,res){
	if(req.session && req.session.auth == true){
		 var data =req.body;
		 data.appointmentId=req.session.login_id;
		 var insertdata = await appointment.create(data);
		 if(insertdata){
			req.flash('msg','User Add Successfully')
			res.redirect('/appointment');
		 }
	}else{ 
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
},
delete_appointment: async function (req,res){
		const dlt = await appointment.destroy({
			where: {
			  id: req.body.id
			}
		  });
     res.json(1);
},
update_status:async function(req,res){
		try{
			var id=req.body.id;
			var appointments = await appointment.findOne({
			where : {
			id:id
			}
			});
		var st ='';
		if(appointments.dataValues.status==1){
			st =0;
		}else{
			st =1;
		}
		// update query ---------------------
		var appointment_update = await appointment.update(
			{ 
				status:st
			},
		{ 
		where: 
			{ 
			id: id 
			} 
			}
		);
		res.json(st);
		}catch(error){
			throw(error);
			}
	
		},

edit: async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var id= req.query.id;
		appointment_data = await appointment.findOne({
			where:{
               id:id
 			}
		});
		if(appointment_data){
			appointment_data=appointment_data.toJSON();
			res.render("dashboard/appointment/edit",{msg:req.flash('msg'),response:appointment_data,title:'appointment',session:req.session});	
		}else{
			req.flash('msg', 'User Not Found.')
			res.redirect('/appointment');
		}
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}	

	}catch(error){

		throw(error);
	}
},
appointment_edit:async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var requestdata = req.body;
		var id =requestdata.id;
		// var appointments = await appointment.findOne({
		// 	where : {
		// 	id:requestdata.id
		// 	}
		// 	});
		delete requestdata.id;
		var appointment_update = await appointment.update(
		   requestdata,
			{ 
				where: 
				{ 
					id: id 
				} 
			}
		);
		req.flash('msg', 'appointment updated successfully')

		res.redirect('/appointment')
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}		

	}catch(error){
		throw(error)
	}

},

}
