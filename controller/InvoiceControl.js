const db= require('../models');
const sequelize=require('sequelize');
var crypto=require('crypto');
const flash = require('connect-flash');
const invoice =db.invoice;
const client =db.client;
const country =db.countries;
const items =db.items;
const user_bank =db.userBank;
const user_expense =db.userexpense;

invoice.belongsTo(client, {
    foreignKey: 'clientId'
});
module.exports={

index: async function(req,res){
	try{
		if(req.session && req.session.auth == true){
		var get_invoice = await invoice.findAll({
			where:{
				userId:req.session.login_id
			},
			include:[{
				model:client,
				attributes:['id','name'],
				required:false
			}]

		});
		if(get_invoice){
			get_invoice = get_invoice.map(val=>{return val.toJSON()});
		}
		res.render("dashboard/invoice/index",{title:'invoice',msg:req.flash('msg'),data:get_invoice,session:req.session});	
	}else{
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
		}catch(error){
		throw(error);
	}
},

add: async function (req,res){
	if(req.session && req.session.auth == true){
		var get_client = await client.findAll({
			where:{
				userId:req.session.login_id
			},
			raw:true
		});

		var get_items = await items.findAll({
			where:{
				userId:req.session.login_id
			},
			raw:true
		});
		var get_bank = await user_bank.findAll({
			where:{
				userId:req.session.login_id
			},
			raw:true
		});

		var get_expense = await user_expense.findAll({
			where:{
				userId:req.session.login_id
			},
			raw:true
		});
		//console.log('hiiiiiiii',get_expense);
    var final_data ={
		 user_bank:get_bank,
		 user_expense:get_expense,
		 user_items:get_items,
		 user_client:get_client
	 }
		//console.log(get_client,"====================get_client",get_items,"=============");
		res.render("dashboard/invoice/add",{title:'invoice',msg:req.flash('msg'), response:final_data,session:req.session});
	}else{ 
	req.flash('msg','Please login first')
     res.redirect('/home');
	}
},

invoice_add: async function (req,res){
	if(req.session && req.session.auth == true){
		 var data =req.body;
		 data.userId=req.session.login_id;
		 var insertdata = await invoice.create(data);
		 if(insertdata){
			req.flash('msg','User Add Successfully')
			res.redirect('/invoice');
		 }
	}else{ 
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
},
delete_invoice: async function (req,res){
		const dlt = await invoice.destroy({
			where: {
			  id: req.body.id
			}
		  });
     res.json(1);
},
update_status:async function(req,res){
		try{
			var id=req.body.id;
			var invoice = await invoice.findOne({
			where : {
			id:id
			}
			});
		var st ='';
		if(invoice.dataValues.status==1){
			st =0;
		}else{
			st =1;
		}
		// update query ---------------------
		var invoice_update = await invoice.update(
			{ 
				status:st
			},
		{ 
		where: 
			{ 
			id: id 
			} 
			}
		);
		res.json(st);
		}catch(error){
			throw(error);
			}
	
		},

edit: async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var id= req.query.id;
		invoice_data = await invoice.findOne({
			where:{
               id:id
			 },
			 raw:true
		});

		var get_client = await client.findAll({
			where:{
				userId:req.session.login_id
			},
			raw:true
		});

		var get_items = await items.findAll({
			where:{
				userId:req.session.login_id
			},
			raw:true
		});
		var get_bank = await user_bank.findAll({
			where:{
				userId:req.session.login_id
			},
			raw:true
		});

		var get_expense = await user_expense.findAll({
			where:{
				userId:req.session.login_id
			},
			raw:true
		});
		var final_data ={
			user_bank:get_bank,
			user_expense:get_expense,
			user_items:get_items,
			user_client:get_client,
			invoice_data:invoice_data
		}
		if(final_data){
			res.render("dashboard/invoice/edit",{msg:req.flash('msg'),response:final_data,title:'invoice',session:req.session});	
		}else{
			req.flash('msg', 'User Not Found.')
			res.redirect('/invoice');
		}
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}	

	}catch(error){

		throw(error);
	}
},
invoice_edit:async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var requestdata = req.body;
		var id =requestdata.id;
		var invoice = await invoice.findOne({
			where : {
			id:requestdata.id
			}
			});
			delete requestdata.id;
		var invoice_update = await invoice.update(
		   requestdata,
			{ 
				where: 
				{ 
					id: id 
				} 
			}
		);
		req.flash('msg', 'invoice updated successfully')

		res.redirect('/invoice')
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}		

	}catch(error){
		throw(error)
	}

},

}
