const db= require('../models');
const sequelize=require('sequelize');
var crypto=require('crypto');
const flash = require('connect-flash');
const fun = require('../function/function.js');
const expense =db.userexpense;

module.exports={

index: async function(req,res){
	try{
		if(req.session && req.session.auth == true){
		var get_expense = await expense.findAll();
		if(get_expense){
			get_expense =get_expense.map(val=>{return val.toJSON()});
		}
		res.render("dashboard/expense/index",{title:'expense',msg:req.flash('msg'),data:get_expense,session:req.session});	
	}else{
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
		}catch(error){
		throw(error);
	}
},

add: async function (req,res){
	if(req.session && req.session.auth == true){
		res.render("dashboard/expense/add",{title:'expense',msg:req.flash('msg'),session:req.session});
	}else{ 
	req.flash('msg','Please login first')
     res.redirect('/home');
	}
},

expense_add: async function (req,res){
	if(req.session && req.session.auth == true){
		 var data =req.body;
		 data.userId=req.session.login_id;
		 data.status =1;
		 var photo ='';
		 if(req.files && req.files.image){
			 photo = await fun.single_image_upload(req.files.image,'upload');
		 }
		 data.photo =photo;
		 var insertdata = await expense.create(data);
		 if(insertdata){
			req.flash('msg','expense Add Successfully')
			res.redirect('/expenses');
		 }
	}else{ 
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
},
delete_expense: async function (req,res){
		const dlt = await expense.destroy({
			where: {
			  id: req.body.id
			}
		  });
     res.json(1);
},
update_status:async function(req,res){
		try{
			var id=req.body.id;
			var expensess = await expense.findOne({
			where : {
			id:id
			}
			});
		var st ='';
		if(expensess.dataValues.status==1){
			st =0;
		}else{
			st =1;
		}
		// update query ---------------------
		var expense_update = await expense.update(
			{ 
				status:st
			},
		{ 
		where: 
			{ 
			id: id 
			} 
			}
		);
		res.json(st);
		}catch(error){
			throw(error);
			}
	
		},

edit: async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var id= req.query.id;
		expense_data = await expense.findOne({
			where:{
               id:id
 			}
		});
		if(expense_data){
			expense_data=expense_data.toJSON();
			res.render("dashboard/expense/edit",{msg:req.flash('msg'),response:expense_data,title:'expense',session:req.session});	
		}else{
			req.flash('msg', 'expense Not Found.')
			res.redirect('/expenses');
		}
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}	

	}catch(error){

		throw(error);
	}
},
expense_edit:async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var requestdata = req.body;
		var id =requestdata.id;
		var expenses = await expense.findOne({
			where : {
			id:requestdata.id
			}
			});
			delete requestdata.id;
			requestdata.photo =expenses.dataValues.photo;
			if(req.files && req.files.image){
				photo = await fun.single_image_upload(req.files.image,'upload');
				requestdata.photo =photo;
			}
			
		var expense_update = await expense.update(
		   requestdata,
			{ 
				where: 
				{ 
					id: id 
				} 
			}
		);
		req.flash('msg', 'expense updated successfully')

		res.redirect('/expenses')
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}		

	}catch(error){
		throw(error)
	}

},

}
