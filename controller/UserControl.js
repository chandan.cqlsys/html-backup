const db= require('../models');
const sequelize=require('sequelize');
var crypto=require('crypto');
const flash = require('connect-flash');
const fun = require('../function/function.js');
const user =db.users;
const country =db.countries;
const client =db.client;
const expense =db.userexpense;
const item =db.items;
const user_bank =db.userBank;
module.exports={
GetCountry: async  function(req,res){
	try{	
          var get_data = await country.findAll({
			  attributes:['id','name']
		  });
          if(get_data){
			get_data =get_data.map(val=>{return val.toJSON()});
		  }
	      return  res.json(get_data);
	}catch(error){
		throw(error);
		}
},

RegisterUser : async function(req,res){
	try{
		var requestdata =req.body;
		//console.log(requestdata,"==============requestdata");
		requestdata.password =	crypto.createHash('sha1').update(requestdata.password).digest('hex');
		 var insert_data = await user.create(requestdata);
		 if(insert_data){
			res.redirect('dashboard/login');
		 }

	}catch(error){
		throw(error);
	}

},
Login: function(req,res){
	try{
		res.render("dashboard/login",{title:'login',msg:req.flash('msg')});	
		}catch(error){
		throw(error);
	}
},

UserLogin:  async function(req,res){
	try{
		//console.log(req.body,"==================req.body");
		const confirm_password = crypto.createHash('sha1').update(req.body.password).digest('hex');
		const users = await user.findOne({
											where : {
														email : req.body.email,
														password:confirm_password,
														accountType:0
													}
											});
		if(users)
		{
			res.session =req.session;
			req.session.email =users.dataValues.email;
			req.session.name =users.dataValues.name;
			req.session.image =users.dataValues.image;
			req.session.logo =users.dataValues.logo;
			req.session.id =users.dataValues.id;
			req.session.login_id =users.dataValues.id;
			res.session.auth = true;
			req.flash('msg', 'Login Successfully.')
			res.redirect('/dashboard');
		}
		}catch(error){
		throw(error);
	}
},

Dashboard: async function(req,res){
	try{

		if(req.session && req.session.auth==true)
		{
			var get_user = await client.count({
				where:{
					userId:req.session.login_id
				}
			});
			var expensive = await expense.count({
				where:{
					userId:req.session.login_id
				}
			});

			var get_items = await item.count({
				where:{
					userId:req.session.login_id
				}
			});
			var get_bank = await user_bank.count({
				where:{
					userId:req.session.login_id
				}
			});
			var final={
				user:get_user,
				expensive:expensive,
				item:get_items,
				bank:get_bank
			}
		 res.render("dashboard/dashboard/index",{title:'dashboard',msg:req.flash('msg'),session:req.session,data:final});	
		}else{
			req.flash('msg', 'Please login first')
			res.redirect('/home');
		}
		}catch(error){
		throw(error);
	}
},

profile: async function(req,res){
	try{

		if(req.session && req.session.auth==true)
		{
			//console.log(req.session.login_id,"====================");
			var get_data = await user.findAll({
				where:{
					id:req.session.login_id
				}
			});
			if(get_data){
				get_data =get_data.map(val=>{return val.toJSON()});
			}
		 res.render("dashboard/admin/index",{title:'profile',msg:req.flash('msg'),data:get_data,session:req.session});	
		}else{
			req.flash('msg', 'Please login first')
			res.redirect('/home');
		}
		}catch(error){
		throw(error);
	}
},

Logout: async function (req,res){
 
	if(req.session && req.session.auth == true){
	  /*console.log("yhasduydg");
  */    req.session.id="";
		req.session.email="";
		req.session.auth=false;

			  req.flash('msg', 'Logout Successfully.')
			  res.redirect('/home');
	  
	}else{
  res.redirect('/home');
}

},

edit: async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var id= req.query.id;
		user_data = await user.findOne({
			where:{
               id:id
 			}
		});
		if(user_data){
			user_data=user_data.toJSON();
			res.render("dashboard/admin/edit",{msg:req.flash('msg'),response:user_data,title:'admin',session:req.session});	
		}else{
			req.flash('msg', 'admin Not Found.')
			res.redirect('/dashboard');
		}
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}	

	}catch(error){

		throw(error);
	}
},

admin_edit:async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var requestdata = req.body;
		var id =requestdata.id;
		var users = await user.findOne({
			where : {
			id:requestdata.id
			}
			});
			delete requestdata.id;
			requestdata.image =users.dataValues.image;
			if(req.files && req.files.image){
				photo = await fun.single_image_upload(req.files.image,'upload');
				requestdata.image =photo;
			}

			console.log(requestdata,"==============requestdata");
		var user_update = await user.update(
		   requestdata,
			{ 
				where: 
				{ 
					id: id 
				} 
			}
		);
req.session.image= requestdata.image;
req.session.name= requestdata.name;
		req.flash('msg', 'admin updated successfully')

		res.redirect('/dashboard')
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}		

	}catch(error){
		throw(error)
	}

},


}
