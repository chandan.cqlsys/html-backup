const db= require('../models');
const sequelize=require('sequelize');
const flash = require('connect-flash');
module.exports={
index: function(req,res){
		try{	
		      res.render("login/home",{title:'home',msg:req.flash('msg')});	
			}catch(error){
			throw(error);
		}
},
feature: function(req,res){
	try{
		  res.render("login/feature",{title:'feature',msg:req.flash('msg')});	
		}catch(error){
		throw(error);
	}
},
plans: function(req,res){
	try{
		  res.render("login/plan",{title:'plan',msg:req.flash('msg')});	
		}catch(error){
		throw(error);
	}
},
about: function(req,res){
	try{	
		  res.render("login/about",{title:'about',msg:req.flash('msg')});	
		}catch(error){
		throw(error);
	}
},
support: function(req,res){
	try{	
		  res.render("login/support",{title:'support',msg:req.flash('msg')});	
		}catch(error){
		throw(error);
	}
},

}
