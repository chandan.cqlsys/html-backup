const db= require('../models');
const sequelize=require('sequelize');
var crypto=require('crypto');
const flash = require('connect-flash');
const user =db.user;
const client =db.client;
const country =db.countries;
module.exports={

index: async function(req,res){
	try{
		if(req.session && req.session.auth == true){
		var get_client = await client.findAll({
			where:{
				userId:req.session.login_id
			}
		});
		if(get_client){
			get_client =get_client.map(val=>{return val.toJSON()});
		}
		res.render("dashboard/user/index",{title:'user',msg:req.flash('msg'),data:get_client,session:req.session});	
	}else{
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
		}catch(error){
		throw(error);
	}
},

add: async function (req,res){
	if(req.session && req.session.auth == true){
		res.render("dashboard/user/add",{title:'user',msg:req.flash('msg'),session:req.session});
	}else{ 
	req.flash('msg','Please login first')
     res.redirect('/home');
	}
},

user_add: async function (req,res){
	if(req.session && req.session.auth == true){
		 var data =req.body;
		 data.userId=req.session.login_id;
		 var insertdata = await client.create(data);
		 if(insertdata){
			req.flash('msg','User Add Successfully')
			res.redirect('/users');
		 }
	}else{ 
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
},
delete_user: async function (req,res){
		const dlt = await client.destroy({
			where: {
			  id: req.body.id
			}
		  });
     res.json(1);
},
update_status:async function(req,res){
		try{
			var id=req.body.id;
			var userss = await client.findOne({
			where : {
			id:id
			}
			});
		var st ='';
		if(userss.dataValues.status==1){
			st =0;
		}else{
			st =1;
		}
		// update query ---------------------
		var user_update = await client.update(
			{ 
				status:st
			},
		{ 
		where: 
			{ 
			id: id 
			} 
			}
		);
		res.json(st);
		}catch(error){
			throw(error);
			}
	
		},

edit: async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var id= req.query.id;
		user_data = await client.findOne({
			where:{
               id:id
 			}
		});
		if(user_data){
			user_data=user_data.toJSON();
			res.render("dashboard/user/edit",{msg:req.flash('msg'),response:user_data,title:'user',session:req.session});	
		}else{
			req.flash('msg', 'User Not Found.')
			res.redirect('/users');
		}
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}	

	}catch(error){

		throw(error);
	}
},
user_edit:async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var requestdata = req.body;
		var id =requestdata.id;
		var users = await client.findOne({
			where : {
			id:requestdata.id
			}
			});
			delete requestdata.id;
		var user_update = await client.update(
		   requestdata,
			{ 
				where: 
				{ 
					id: id 
				} 
			}
		);
		req.flash('msg', 'user updated successfully')

		res.redirect('/users')
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}		

	}catch(error){
		throw(error)
	}

},

}
