const db= require('../models');
const sequelize=require('sequelize');
var crypto=require('crypto');
const flash = require('connect-flash');
const item =db.items;

module.exports={

index: async function(req,res){
	try{
		if(req.session && req.session.auth == true){
		var get_item = await item.findAll();
		if(get_item){
			get_item =get_item.map(val=>{return val.toJSON()});
		}
		res.render("dashboard/item/index",{title:'item',msg:req.flash('msg'),data:get_item,session:req.session});	
	}else{
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
		}catch(error){
		throw(error);
	}
},

add: async function (req,res){
	if(req.session && req.session.auth == true){
		res.render("dashboard/item/add",{title:'item',msg:req.flash('msg'),session:req.session});
	}else{ 
	req.flash('msg','Please login first')
     res.redirect('/home');
	}
},

item_add: async function (req,res){
	if(req.session && req.session.auth == true){
		 var data =req.body;
		 data.userId=req.session.login_id;
		 data.status =1;
		 var insertdata = await item.create(data);
		 if(insertdata){
			req.flash('msg','item Add Successfully')
			res.redirect('/items');
		 }
	}else{ 
		req.flash('msg','Please login first')
		res.redirect('/home');
	}
},
delete_item: async function (req,res){
		const dlt = await item.destroy({
			where: {
			  id: req.body.id
			}
		  });
     res.json(1);
},
update_status:async function(req,res){
		try{
			var id=req.body.id;
			var itemss = await item.findOne({
			where : {
			id:id
			}
			});
		var st ='';
		if(itemss.dataValues.status==1){
			st =0;
		}else{
			st =1;
		}
		// update query ---------------------
		var item_update = await item.update(
			{ 
				status:st
			},
		{ 
		where: 
			{ 
			id: id 
			} 
			}
		);
		res.json(st);
		}catch(error){
			throw(error);
			}
	
		},

edit: async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var id= req.query.id;
		item_data = await item.findOne({
			where:{
               id:id
 			}
		});
		if(item_data){
			item_data=item_data.toJSON();
			res.render("dashboard/item/edit",{msg:req.flash('msg'),response:item_data,title:'item',session:req.session});	
		}else{
			req.flash('msg', 'item Not Found.')
			res.redirect('/items');
		}
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}	

	}catch(error){

		throw(error);
	}
},
item_edit:async function(req,res){
	try{
		if(req.session && req.session.auth==true){
		var requestdata = req.body;
		var id =requestdata.id;
		var items = await item.findOne({
			where : {
			id:requestdata.id
			}
			});
			delete requestdata.id;
		var item_update = await item.update(
		   requestdata,
			{ 
				where: 
				{ 
					id: id 
				} 
			}
		);
		req.flash('msg', 'item updated successfully')

		res.redirect('/items')
	}else{
		req.flash('msg', 'Please login first')
		res.redirect('/home');
	}		

	}catch(error){
		throw(error)
	}

},

}
